import ballerina/io;
import ballerina/kafka;
//import ballerina/lang.'string;
import ballerina/log;

kafka:ConsumerConfiguration consConf = {
    bootstrapServers: "localhost:9092",
    groupId: "group-id",

    topics: ["dsp"],
    pollingIntervalInMillis: 1000,
    keyDeserializerType: kafka:DES_INT,
    valueDeserializerType: kafka:DES_STRING,
    autoCommit: false
};

listener kafka:Consumer cons = new (consConf);

service kafkaService on cons {
	resource function onMessage(kafka:Consumer kafkaConsumer, kafka:ConsumerRecord[] records) {
		foreach var rec in records
		{
			processKafkaRecord(rec);
		}

		var commitResult = kafkaConsumer->commit();
		if (commitResult is error) {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", commitResult);
        }
	}
}

function processKafkaRecord(kafka:ConsumerRecord rec) {
	anydata msgVal = rec.value;

	if(msgVal is string)
	{
		io:println("topc: " + rec.topic + " received message " + msgVal);
	}
}