// import ballerina/io;
import ballerina/graphql;
import ballerinax/kafka;

kafka:ProducerConfiguration producerConfiguration = {
    bootstrapServers: "localhost:9092",

    clientId: "basic-producer",
    acks: "all",
    retryCount: 3
};

kafka:Producer kafkaProducer = checkpanic new (producerConfiguration);

service graphql:Service /votoapi on new graphql:Listener(9090) {
    resource function get share(string msg) returns string {
        kafka:ProducerError? sendRes = kafkaProducer->sendProducerRecord({topic: "dsp",
                                value: msg.toBytes() });
        if (sendRes is error) {
            return "An error occurred while sending from the producer...";
        }

        kafka:ProducerError? flushRes = kafkaProducer->flushRecords();
        if (flushRes is error) {
            return "An error occurred while flushing the producer records...";
        }

        return "Acknowledging voter registration...";
    }
}
