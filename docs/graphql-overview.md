GraphQL
========

# What is GraphQL?
[GraphQL](https://graphql.org) is a query language for the application programming interfaces (API) that you use to access and manipulate your complex applications, including distributed systems. To create a GraphQL service one has to define types and fields within those types followed by functions to extract information and manipulate them. Documentation on how to define queries and types in GraphQL can be found on the website.


# Integration with Ballerina
GraphQL comes with a collection of tools, including servers, clients and additional functionality for a set of languages. However, there is no built-in tooling for the Ballerina language. Recently, a new module was introduced to implement GraphQL service in Ballerina. The following [link](https://ballerina.io/swan-lake/learn/by-example/graphql-resources-with-scalar-values.html) provides an example of how to create a GraphQL service in Ballerina. More examples can be found on the website.

You should note that these examples only work under the **Swan Lake** distribution of Ballerina. You can find the instructions to update your Ballerina to Swan Lake distribution on the installation page of Ballerina. In short, the steps are as follows:
* update ballerina `ballerina update`
* pull the swan lake distribution (currently the preview 8 is the latest version) `ballerina dist pull slp8`
* set ballerina to use slp8 `ballerina dist use slp8`