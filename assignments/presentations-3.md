DSP620S Assignment 3 Presentation Schedule
===========================================

In this document, we compile the presentation notes for each group.

# Presentation time slots

| Date | Time slot | Group |
|------|-----------|-------|
|08/02/2021 | 09:00 - 09:20 |  |
|00/02/2021 | 09:20 - 09:40 |  |
|08/02/2021 | 09:40 - 10:00 |  |
|08/02/2021 | 10:00 - 10:20 |  |
|08/02/2021 | 10:20 - 10:40 |  |
|08/02/2021 | 10:40 - 11:00 |  |
|08/02/2021 | 11:00 - 11:20 |  |
|08/02/2021 | 11:20 - 11:40 |  |
|08/02/2021 | 11:40 - 12:00 |  |
|08/02/2021 | 12:00 - 12:20 |  |
|08/02/2021 | 12:20 - 12:40 |  |
|08/02/2021 | 12:40 - 13:00 |  |
|08/02/2021 | 13:00 - 13:20 |  |
|09/02/2021 | 10:20 - 10:40 | Group 10 |
|09/02/2021 | 12:00 - 12:20 | Group 8 |
|09/02/2021 | 11:40 - 12:00 | Group 13 |
|09/02/2021 | 16:20 - 16:40 | Group |
|10/02/2021 | 12:00 - 12:20 | Group 3 |
|10/02/2021 | 13:00 - 13:20 | Group 7 |
|10/02/2021 | 17:00 - 17:20 | Group 4 |
|10/02/2021 | 17:40 - 18:00 | Group 5 |
|11/02/2021 | 09:20 - 09:40 | Group 6 |
|11/02/2021 | 10:00 - 10:20 | Group 1 |
|11/02/2021 | 10:40 - 11:00 | Group 15 |
|11/02/2021 | 11:20 - 11:40 | Group 16 |
|11/02/2021 | 16:20 - 16:40 | Group 11 |
|11/02/2021 | 16:40 - 17:00 | Group 2 |
|11/02/2021 | 17:20 - 17:40 | Group 9 |
|11/02/2021 | 18:00 - 18:20 | Group 14 |
|12/02/2021 | 09:00 - 09:20 | Group 18 |
|12/02/2021 | 10:40 - 11:00 | Group 13 |
|12/02/2021 | 11:20 - 11:40 | Group 20 |
|12/02/2021 | 16:20 - 16:40 | Group 17 |
|12/02/2021 | 17:00 - 17:20 | Group 19 |
|12/02/2021 | 18:40 - 19:00 | Group 21 |
|15/02/2021 | 10:00 - 10:20 | Group 22 |


# Evaluation criteria

The following criteria will be followed to assess each submission (Note that the mark allocation is tentative)

* Problem decomposition into services (10%)
* API gateway design and implementation with graphql integration (20%) 
* Implementation of the services and their deployment with containerisation and orchestration (50%)
* The business logic inside each service. (20%)
* running implementation



# Notes on group presentations

## Group 1

- Repository: [Submission](https://github.com/fadedphantom/DSP_Assignment3)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Michael Apata | 219050449 | FT |

- Presentation
	- Date: 11/02/2021
	- Time: 10:00 - 10:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
			- side note: although the initial team members decided to part ways, they only inform this member this morning
		
		
		- microservice design
			- only two services were implemented
			- could not clearly explain the rationale behind how service boundaries are defined
		- API gateway and API query with graphql
			- no proper API gateway
			- there's a separate graphql implementation outside the Ballerina implementation
		- logic within each service
			- nothing noteworthy
		- communication paradigm betweeen servies
			- there is a Kafka setup
			- but I can't see a producer and a consumer communicate
		- deployment with Kubernetes and Docker
			- no
		- running implementation
			- yes, but partially
			- could issue requests with CURL for some part of the project
- Mark:

## Group 2

- Repository: [Submission](https://github.com/hpmouton/assignment-3)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Vetara Kaurimuje | 219116458 | FT |
| Vemuna Kaurimuje | 219048355 | FT |
| Hubert Mouton | 219079714 | FT |
| Maria Thomas | 219063060 | FT |

- Presentation
	- Date: 11/02/2021
	- Time: 16:40 - 17:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- the decomposition
		- API gateway and API query with graphql
			- Comments
		- logic within each service
			- Comments
		- communication paradigm betweeen servies
			- comments
		- deployment with Kubernetes and Docker
			- Comments
		- running implementation
			- yes/No
- Mark:


## Group 3

- Repository: [Submission](https://github.com/oUchezuba/assignment3)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Owen Uchezuba | 219038309 | FT |
| De Almeida Reis L. R. | 219081662 | FT |
| Jurema Martins | 219060789 | FT |

- Presentation
	- Date: 10/02/2021
	- Time: 12:00 - 12:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- implemented several services
			- could not clearly explain the guiding principle of their microservice definition
		- API gateway and API query with graphql
			- no API gateway (seems to confuse it with the communication paradigm)
		- logic within each service
			- used MySQL DB for persistence (but centralised)
		- communication paradigm betweeen servies
			- used Kafka for communication between services
			- only one consumer and one producer
		- deployment with Kubernetes and Docker
			- not implemented
		- running implementation
			- no
- Mark:

## Group 4

- Repository: [Submission](https://github.com/Sergio-Jim/DSP_Assignment3.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Jimmy Sergio Damiao | 218090013 | PT |
| Eugene Mwewa | 218040245 | FT |
| Hena Sibalatani | 219153566 | FT |
| Imelda Haufiku | 219065128 | FT|

- Presentation
	- Date: 10/02/2021
	- Time: 17:00 - 17:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- seeral services were implemented
			- the guiding principle is not clear (roles/functions, stages, etc???)
		- API gateway and API query with graphql
			- offered menu items then converted into GraphQL queries
		- logic within each service
			- nothing noteworthy
		- communication paradigm betweeen servies
			- used Kafka with a consumer and a producer per service
		- deployment with Kubernetes and Docker
			- attempted but failed
		- running implementation
			- yes
- Mark:

## Group 5

- Repository: [Submission](https://github.com/Takunda_Hwaire/dsp_assignment3.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Isaac Makosa | 219005516 | FT |
| Takunda Hwaire | 219155380 | FT |
| Tafadzwa Taringa | 219003068 | FT |
| Adilson da Costa | 219020124 | FT |

- Presentation
	- Date: 10/02/2021
	- Time: 17:40 - 18:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- several services
			- decomposed services by stage
		- API gateway and API query with graphql
			- graphql but not properly implemented
		- logic within each service
			- used a centralised db for persistence
		- communication paradigm betweeen servies
			- used Kafka for communication between services
		- deployment with Kubernetes and Docker
			- attempted but failed
		- running implementation
			- No (could build, but not run)
- Mark:


## Group 6

- Repository: [Submission](https://github.com/Qronus/microservices)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Olivier Lee-Roy | 218113951 | FT |
| Hango Ndeyapo | 218064187 | FT |
| Shilongo Stephen | 218059299 | FT |
| Bill Ikela | 218099509 | FT |

- Presentation
	- Date: 11/02/2021
	- Time: 09:20 - 09:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 7/02/2021
		
		
		- microservice design
			- Decomposed services by roles
			  - Fie service
			  - HDC service
			  - HOD service
			  - Student Service
			  - Supervisor Service
			  - All the services are fully implemented 
		- API gateway and API query with graphql
			- Implemented and used by the client to query the kafka
		- logic within each service
			- Correct
			- data is being saved to the json file
		- communication paradigm betweeen servies
			- The services communicates to each other through Kafka.
		- deployment with Kubernetes and Docker
			- implemented by not utilised 
		- running implementation
			- yes
- Mark:

## Group 7

- Repository: [Submission](https://github.com/Sacky061/DSP_3rd_Project)


- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| L I Petrus | 217070418 | FT |
| A.K Kudumo| 217005195 | PT |
| Metumo N Shifindi | 219056676 | FT |
| S.S Simon | 213031272 | PT |

- Presentation
	- Date: 10/02/2021
	- Time: 13:00 - 13:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 7/02/2021
		
		
		- microservice design
			- Application Service
			- viewApplication Service
			- view Application service
			- Application Update Service
			- interviewinvite Application
			- GraphQL service
			- Proposal service
			- Submit Result Service
			- ThesisSubmit Service
		- API gateway and API query with graphql
			- The graphql is implemented to query mySQL database
		- logic within each service
			- correct
		- communication paradigm betweeen servies
			- each service is consisted of the consumer and Producer
		- deployment with Kubernetes and Docker
			- implemented but not running
		- running implementation
			- No
- Mark:


## Group 8

- Repository: [Submission](https://gitlab.com/kanengonitinashe17/dsp-assignment-3)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Tinashe Kanengoni | 219034559 | PT |
| Antonio Reinecke | 219143218 | FT |
| Lemuel Mayinoti | 219156786 | FT |

- Presentation
	- Date: 09/02/2021
	- Time: 12:00 - 12:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 7/02/2021
		
		
		- microservice design
			- HOD service
			- FIE service
			- HDC Service 
			- Supervisor service
			- Student service
		- API gateway and API query with graphql
			- Implemented an api gate way for each service 
			- Consumer querys the graphql
			- graphql api gate way is being used to send information to the producers
		- logic within each service
			- The service is consist of a consumer and producers 
			- Consumer pass the data to the producer through graphql
		- communication paradigm betweeen services
			- The services communicates to each others the kafka by subscribing to the kafka topics
			- data is being shared between the services
			- no actual files is being passed between the services
			- Json file is being created for each student upon registering.
		- deployment with Kubernetes and Docker
			- Implemented but displaying cannot connect to host error
		- running implementation
			- yes
- Mark:


## Group 9

- Repository: [Submission]()

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Eliakim Haufiku | 215014928 | FT |
| Germias Mwafangeyo | 218060750 | FT |

- Presentation
	- Date: 11/02/2021
	- Time: 17:20 - 17:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- no attempt
		- API gateway and API query with graphql
			- not working
		- logic within each service
			- nothing noteworthy
		- communication paradigm betweeen services
			- Kafka failed
		- deployment with Kubernetes and Docker
			- failed attempt
		- running implementation
			- partial (only menu is displayed)
- Mark:

## Group 10

- Repository: [Submission](https://gitlab.com/kauly-dot-repo/DSP-Assignment-3)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Kaulyaalalwa Peter | 219143765 | FT |
| Emanuel A. Q. Vuma | 219091617 | FT |

- Presentation
	- Date: 09/02/2021
	- Time: 10:20 - 10:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- services decomposition following the need for communication instead of roles or actions or domains
		- API gateway and API query with graphql
			- menu items used to generate GraphQL queries
		- logic within each service
			- correct
		- communication paradigm betweeen servies
			- Kafka deployment with producers and consumers for each type of service
		- deployment with Kubernetes and Docker
			- attempted but had issues
		- running implementation
			- yes
- Mark:

## Group 11

- Repository: [Submission](https://gitlab.com/kanengonitinashe17/dsp-assignment-3)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Vilho Nguno | 219068208 | FT |
| Vesta Ngomberume | 219085803 | FT |
| Vijanda V. Herunga | 216016681 | FT |
| Shania Nkusi | 219051704 | FT |

- Presentation
	- Date: 11/02/2021
	- Time: 16:20 - 16:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- the breakdown into services ws closer to a split per stage
			- 5 microservices were thought of
		- API gateway and API query with graphql
			- attempted a graphql implementation but not fully integrated
		- logic within each service
			- nothing noteworthy
		- communication paradigm betweeen servies
			- there was a Kafka deployment with one consumer and one producer
		- deployment with Kubernetes and Docker
			- failed attempt
		- running implementation
			- No
- Mark:

## Group 12

- Repository: [Submission](https://gitlab.com/consuelo.ogbokor/dsp-assignment-3)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Elo Gloria Ogbokor | 216062195 | FT |

- Presentation
	- Date: 11/02/2021
	- Time: 18:40 - 19:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- not clear
		- API gateway and API query with graphql
			- implemented a graphql but outside Ballerina
		- logic within each service
			- nothing noteworthy
		- communication paradigm betweeen servies
			- implemented one producer and one consumer
		- deployment with Kubernetes and Docker
			- attempted but got error
		- running implementation
			- partial
			- most components are not integrated into one whole
- Mark:


## Group 13

- Repository: [Submission](https://gitlab.com/ronaldsangunji/dsp-project-3)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Nestor Nathingo | 219051844 | FT |
| Sakaria Shilikomwenyo | 219055572 | FT |
| Simeon Hifikepunye | 219047987 | FT |
| Ronald Sangunji | 217069649 | FT |

- Presentation
	- Date: 12/02/2021
	- Time: 10:40 - 11:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- only big service (no micro-service)
		- API gateway and API query with graphql
			- very partial implementation of GraphQL
		- logic within each service
			- no comment
		- communication paradigm betweeen servies
			- Kafka with two consumers and one producer
		- deployment with Kubernetes and Docker
			- No
		- running implementation
			- No (only compilation)
- Mark:

## Group 15

- Repository: [Submission](https://github.com/MorrisVatileni123/DSP-Assignment-3.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Morris Vatileni | 217090044 | FT |
| David Ingashipola | 217111904 | FT |
| Johannes Naemi | 214009084 | FT |
| Matheus Julius | 214082474 | FT |

- Presentation
	- Date: 11/02/2021
	- Time: 10:40 - 11:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 7/02/2021
		
		
		- microservice design
			- decomposed services by roles
			   - student
			   - HDC
			   - FIE
			   - HOD
			   - Supervisor
		- API gateway and API query with graphql
			- Implementing and querying to kafka
		- logic within each service
			- correct
		- communication paradigm betweeen servies
			- The services are communicating to each other through kafka
			- function are implemnted till approval of Proposal only
		- deployment with Kubernetes and Docker
			- partially done 
		- running implementation
			- yes
- Mark:

## Group 16

- Repository: [Submission](https://github.com/WillyWonker69/DSP_ClassOf2020_ExclusiveIT-Ass3)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Brave Wilson Kahweka | 219070318 | FT |
| Mino V. P. David | 219079846 | FT |
| Cesario D. C. Alves | 219150494 | FT |
| Oletu M. Shikomba | 219060355 | FT |

- Presentation
	- Date: 11/02/2021
	- Time: 11:20 - 11:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 7/02/2021
		
		
		- microservice design
			- services decomposed by actions
			- fuctions are declared by no definition or Implementation 
		- API gateway and API query with graphql
			- partially done
		- logic within each service
			- no implementation apart from kafka producer setup
		- communication paradigm betweeen servies
			- no communication since the system is barely implemented 
		- deployment with Kubernetes and Docker
			- not implemented
		- running implementation
			- No
- Mark:

## Group 17

- Repository: [Submission](https://github.com/219150354/DSP-assignment-3)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Silas T. Nghuushi | 218118740 | PT |
| Sakeus Nangolo | 219150354 | PT |

- Presentation
	- Date: 11/02/2021
	- Time: 16:20 - 16:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 7/02/2021
		
		
		- microservice design
			- services decomposed by actions
			- fuctions are declared by no definition or Implementation 
		- API gateway and API query with graphql
			- partially done
		- logic within each service
			- no implementation apart from kafka producer setup
		- communication paradigm betweeen servies
			- no communication since the system is barely implemented 
		- deployment with Kubernetes and Docker
			- not implemented
		- running implementation
			- No
- Mark:


## Group 18

- Repository: [Submission](https://github.com/Ndapandula-cyber/Assignment-3)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Tabitha Nehale | 200840851 | FT |
| Shelma Shikongo | 218000146 | FT |
| Uajaa Veii | 200529994 | FT |
| Ndapandula Shiweva | 218072422 | FT |

- Presentation
	- Date: 12/02/2021
	- Time: 09:00 - 09:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 7/02/2021
		
		
		- microservice design
			- several services were identified but the rationale is not quite clear
			- tried to follow the stages in the requirements
		- API gateway and API query with graphql
			- not clear
		- logic within each service
			- no comments
		- communication paradigm betweeen servies
			- used Kafka wih one topic, a producer and three consumers
		- deployment with Kubernetes and Docker
			- not implemented
		- running implementation
			- No
- Mark:


## Group 19

- Repository: [Submission](https://github.com/Rosvita10/Assignment-3.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Kay-Lynne van Wyk | 219114056 |  |
| Rosvita Haikera | 214076008 |  |

- Presentation
	- Date: 12/02/2021
	- Time: 17:00 - 17:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- seems to have used roles to breakdown services
			- identified five services (e.g., student, supervisor, etc.)
		- API gateway and API query with graphql
			- attempted a GraphQL implementation (menu item selections generate the GraphQL query)
		- logic within each service
			- no comments
		- communication paradigm betweeen servies
			- several producers and consumers but the logic of the communication is barely understandable 
		- deployment with Kubernetes and Docker
			- No
		- running implementation
			- No
- Mark:


## Group 20

- Repository: [Submission](https://github.com/hpmouton/assignment-3.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Vetara Kaurimuje  |219116458 |  |
| Vemuna Kaurimuje | 219048355 |  |
| Maria Thomas | 219063060|  |
| Hubert Mouton | 219079714 |  |

- Presentation
	- Date: 12/02/2021
	- Time: 11:20 - 11:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 7/01/2021
		
		
		- microservice design
			- services decomposed by roles
			  - FIE
			  - HDC
			  - HOD
			  - Student
			  - Supervisor
		- API gateway and API query with graphql
			- implemented to communicate to the producer
		- logic within each service
			- each service is made up of the consumer and a producer
		- communication paradigm betweeen servies
			- Services communicate through kafka 
		- deployment with Kubernetes and Docker
			- Implemented 
		- running implementation
			- yes
- Mark:


## Group 21

- Repository: [Submission](https://github.com/hpmouton/assignment-3.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Braulio Andre | 219036810 | |
| Clive Mafwila | 217048269 | |
| Chozi Kudakwashe | 218019157 | |

- Presentation
	- Date: 12/02/2021
	- Time: 18:40 - 19:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
			- 218019157 did not show up
		
		
		- microservice design
			- identified several services (student, hod, supervisor, etc.)
			- each service is presented as a producer
		- API gateway and API query with graphql
			- the design of the gateway is wrong (one gateway per service instead of a central gateway)
		- logic within each service
			- no comments
		- communication paradigm betweeen servies
			- implemented producers and consumers (with Kafka?) 
		- deployment with Kubernetes and Docker
			- failed attempt
		- running implementation
			- No
- Mark:

## Group 22

- Repository: [Submission](https://github.com/josemarlima/DSP_ASSIGNMENT3)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| U Hange | 200512579 | |
| J Lima | 219011397 | |
| R Junias | 219041466 | |

- Presentation
	- Date: 15/02/2021
	- Time: 10:00 - 10:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 7/02/2021
		
		
		- microservice design
			- services decomposed by roles
			  - FIE
			  - HDC
			  - HOD
			  - Student
			  - Supervisor
			  - The services are full implemented
		- API gateway and API query with graphql
			- Implemented for each service
		- logic within each service
			- data is being saved to a Json file
		- communication paradigm betweeen servies
			- communicates through Kafka
		- deployment with Kubernetes and Docker
			- Implemented but not running
		- running implementation
			- yes
- Mark:
