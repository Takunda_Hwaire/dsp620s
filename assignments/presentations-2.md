DSP620S Assignment 2 Presentation Schedule
===========================================

In this document, we compile the presentation notes for each group.

# Presentation time slots

| Date | Time slot | Group |
|------|-----------|-------|
|04/02/2021 | 12:00 - 12:20 |  |
|04/02/2021 | 12:20 - 12:40 |  |
|04/02/2021 | 12:40 - 13:00 | Group 3 |
|04/02/2021 | 17:00 - 17:20 |  |
|04/02/2021 | 17:20 - 17:40 |  |
|04/02/2021 | 17:40 - 18:00 | Group 6 |
|04/02/2021 | 18:00 - 18:20 |  |
|04/02/2021 | 18:20 - 18:40 |  |
|04/02/2021 | 18:40 - 19:00 | Group 2 |
|05/02/2021 | 09:00 - 09:20 |  |
|05/02/2021 | 09:20 - 09:40 |  |
|05/02/2021 | 09:40 - 10:00 |  |
|05/02/2021 | 10:00 - 10:20 |  |
|05/02/2021 | 10:20 - 10:40 |  |
|05/02/2021 | 10:40 - 11:00 |  |
|05/02/2021 | 11:00 - 11:20 |  |
|05/02/2021 | 11:20 - 11:40 |  |
|05/02/2021 | 11:40 - 12:00 |  |
|05/02/2021 | 12:00 - 12:20 |  |
|05/02/2021 | 12:20 - 12:40 | Group 4 |
|05/02/2021 | 12:40 - 13:00 |  |
|05/02/2021 | 13:00 - 13:20 | Group 7 |
|05/02/2021 | 17:40 - 18:00 | Group 8 |
|09/02/2021 | 10:00 - 10:20 | Group 9 |
|09/02/2021 | 11:20 - 11:40 |  |
|09/02/2021 | 16:00 - 16:20 | Group |
|10/02/2021 | 11:20 - 11:40 | Group 11 |
|10/02/2021 | 12:20 - 12:40 |  |
|10/02/2021 | 17:20 - 17:40 | Group 13 |
|10/02/2021 | 18:00 - 18:20 | Group 1 |
|11/02/2021 | 09:00 - 09:20 | Group 14 |
|11/02/2021 | 12:00 - 12:20 | Group 16 |
|11/02/2021 | 12:40 - 13:00 |  |
|11/02/2021 | 16:00 - 16:20 | Group 12 |
|11/02/2021 | 17:00 - 17:20 | Group 15 |
|11/02/2021 | 14:40 - 15:00 | Group 20 |
|11/02/2021 | 17:20 - 17:40 | Group 18 |
|11/02/2021 | 17:40 - 18:00 | Group 19 |
|11/02/2021 | 18:40 - 19:00 | |
|12/02/2021 | 09:40 - 10:00 | Group 5 |
|12/02/2021 | 10:20 - 10:40 | Group 17 |
|12/02/2021 | 11:00 - 11:20 | Group 21 |
|12/02/2021 | 12:00 - 12:20 | Group 4 |
|12/02/2021 | 16:00 - 16:20 | Group 2 |
|12/02/2021 | 16:40 - 17:00 | Group 1 |
|12/02/2021 | 18:00 - 18:20 | Group 7 |
|15/02/2021 | 09:40 - 10:00 | Group 22 |

# Evaluation criteria

The following criteria will be followed to assess each submission

* design **VoTo** following a micro-service architectural style (You will justify your problem decomposition into services);
* design and implement an API gateway using graphql as your API query language;
* deploy and configure Kafka for all communication in the system (services and API gateway);
* deploy the services using containerisation and orchestration (Docker and Kubernetes).
* running implemnentation


# Notes on group presentations

## Group 1

- Repository: [Submission](https://github.com/Rosvita10/Assignment-2.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Kay-Lynne van Wyk | 219114056 |  |
| Rosvita Haikera | 214076008 |  |

- Presentation
	- Date: 12/02/2021
	- Time: 16:40 -17:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- the rationale of the breakdown is not clear
			- somehow identified the following services: candidates, voter number, fraud, etc.
		- API gateway and API query with graphql
			- just an HTTP listener
		- Kafka deployment
			- could not show/explain any communication logic
		- logic within each service
			- no comments
		- deployment with Kubernetes and Docker
			- failed attempt
		- running implementation
			- no
- Mark:

## Group 2

- Repository: [Submission](https://github.com/218118740/VoTo.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Silas T. Nghuushi | 218118740 | PT |
| Sakeus Nangolo | 219150354 | PT |


- Presentation
	- Date: 12/02/2021
	- Time: 16:00 - 16:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- no real services were identitied
			- claimed to have implemented Kafka and docker
		- API gateway and API query with graphql
			- none
		- Kafka deployment
			- implemented a Kafka producer but no communication was shown
		- logic within each service
			- see running implementation section
		- deployment with Kubernetes and Docker
			- failed attempt
		- running implementation
			- partially
			- no new voter registration
			- same voter could vote several times

- Mark:

## Group 3

- Repository: [Submission](https://github.com/Oprah25/DSP-assignmnt-2.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Johannes Naemi N. | 214009084 | PT |
| Nghidinwa Vlatileni N.V. | 217090044 | FT|
| David Ingashipola Andreas | 217111904 | FT |
| Julius Matheus | 214082474 | FT|


- Presentation
	- Date: 04/02/2021
	- Time: 12:40 - 13:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- Registration
			- Candidate
			- Voting
		- API gateway and API query with graphql
			- No proper API gateway
		- Kafka deployment
			- attempted to use Kafka
			- not set up in right way (very odd Kafka usage)
		- logic within each service
			- requests from outside go straight to specific services
			- only one storage for several services
		- deployment with Kubernetes and Docker
			- no attempt to containerise or orchestrate
		- running program
			- partially
			- no integration/coordination between components
- Mark:

## Group 4

- Repository: [Submission](https://github.com/oUchezuba/assignment2)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Owen Uchezuba | 219038309 | FT |
| De Almeida Reis L. R. | 219081662 | FT |
| Jurema Martins | 219060789 | FT |

- Presentation
	- Date: 12/02/2021
	- Time: 12:00 - 12:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- the decomposition
		- API gateway and API query with graphql
			- Comments
		- Kafka deployment
			- Comments
		- logic within each service
			- Comments
		- deployment with Kubernetes and Docker
			- Comments
- Mark:

## Group 5

- Repository: [Submission](https://gitlab.com/kanengonitinashe17/dsp-voto)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Tinashe Kanengoni | 219034559 | PT |
| Antonio Reinecke | 219143218 | FT |
| Dionosius A. Beukes | 217099114 | FT |
| Lemuel Mayinoti | 219156786 | FT|

- Presentation
	- Date: 12/02/2021
	- Time: 09:40 - 10:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- the rationale behind the breakdown into services is not clear
			- created several services (e.g., projected winner, archive results)
		- API gateway and API query with graphql
			- Although a component serves as interface with the client, the concept of API gateway was poorly designed
		- Kafka deployment
			- used Kafka but not correctly
		- logic within each service
			- no Comments
		- deployment with Kubernetes and Docker
			- failed attempt
		- running implementation
			- partially (communication issue with Kafka)
- Mark:

## Group 6

- Repository: [Submission](https://github.com/Sergio-Jim)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Jimmy Sergio Damiao | 218090013 | PT |
| Eugene Mwewa | 218040245 | FT |
| Hena Sibalatani | 219153566 | FT |
| Imelda Haufiku | 219065128 | FT|

- Presentation
	- Date: 04/02/2021
	- Time: 17:40 - 18:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
			-  student 219153566
		
		
		- microservice design
			- VoteProcessor
		- API gateway and API query with graphql
			- attempted but failed
		- Kafka deployment
			- attempted but failed
		- logic within each service
			- Nothing much
		- deployment with Kubernetes and Docker
			- No containerisation or orchestration
		- running implementation
			- No
- Mark:

## Group 7

- Repository: [Submission](https://gitlab.com/consuelo-ogbokor/dsp-assignment-2)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Elo Gloria Ogbokor | 216062195 | FT |

- Presentation
	- Date: 12/02/2021
	- Time: 18:00 - 18:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- very strange decomposition with a producer and a consumer
		- API gateway and API query with graphql
			- implemented GraphQL calls but isolated from the Ballerina code
		- Kafka deployment
			- a producer and a consumer
		- logic within each service
			- no comments
		- deployment with Kubernetes and Docker
			- not working
		- running implementation
			- partially (all scenarios are hard-coded)
- Mark:


## Group 8

- Repository: [Submission](https://gitlab.com/)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Shifidi Metumo N | 219056676 | FT |
| Kudumo A. K. | 217005195 | PT |
| Leornardo Petrus | 217070418 | FT |
| Simon S. S. | 213031272 | PT |

- Presentation
	- Date: 05/02/2021
	- Time: 17:40 - 18:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- Ballot
			- Vote Registration
			- Elect Result (display outcome)
		- API gateway and API query with graphql
			- used node.js to implement a GraphQL client; but no integration with Ballerina
		- Kafka deployment
			- one single producer for all services
		- logic within each service
			- hard-coded candidates
		- deployment with Kubernetes and Docker
			- No real containerisation or orchestration (only the annotation are added for Docker)
		- running implementation
			- No
- Mark:

## Group 9

- Repository: [Submission](https://gitlab.com/kauly-dot-repo/DSP-Assignment2)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Kaulyaalalwa Peter | 219143765 | FT |
| Emanuel A. Q. Vuma | 219091617 | FT |

- Presentation
	- Date: 09/02/2021
	- Time: 10:00 - 10:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- three services overall
			- voter registration, candidate registration, vote (displays the gradual results as the votes are being cast)
		- API gateway and API query with graphql
			- presents user with menu items and then generate GraphQL queries
			- GraphQL API integrated with Kafka producers
			- did not properly handle the synchronous-asynchronous tension between Kafka and GraphQL
		- Kafka deployment
			- with producers and consumers
		- logic within each service
			- vote tally computed in near real-time
		- deployment with Kubernetes and Docker
			- attempted but had issues
		- running implementation
			- yes
- Mark:


## Group 10

- Repository: [Submission](https://gitlab.com/)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Gideon Shilongo | 217133355 |  |

- Presentation
	- Date: 04/02/2021
	- Time: 18:00 - 18:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- One service for all
		- API gateway and API query with graphql
			- attempted to implement an OpenAPI
		- Kafka deployment
			- No Kafka setup
		- logic within each service
			- Nothing much
		- deployment with Kubernetes and Docker
			- attempted to containerise an isolated unit/service (totally misunderstood)
		- running implementation
			- No
- Mark:


## Group 11

- Repository: [Submission](https://github.com/fadedphantom/DSP_Assignment2)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Michael Apata | 219050449 | FT |
| Matheus J. Tangeni | 214082474 | FT |
| Nghidinwa Jason P. G. Du Plessis | 217076459 | FT |

- Presentation
	- Date: 10/02/2021
	- Time: 11:20 - 11:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 30 January 2021
		- Nghidinwa Jason P. G. Du Plessis was not available for the presentation
		
		- microservice design
			- Admin service
			- voting service
			- Consumer service
		- API gateway and API query with graphql
			- graphql is implemented using java scripts
			- used to manipulate data in the json server
		- Kafka deployment
			- Fully implemented with the producer and consumer implemented in ballerina
		- logic within each service
		    - information from the json file are send to the consumer by the producer through kafka
			- hardcoded voting service
			- no information about voting is being processed 
		- deployment with Kubernetes and Docker
			- implemented but not running after the build
		- running implementation
			- yes
- Mark:


## Group 12

- Repository: [Submission](https://github.com/hpmouton/voto.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Vetara Kaurimuje | 219116458 | FT |
| Vemuna Kaurimuje | 219048355 | FT |
| Hubert Mouton | 219079714 | FT |
| Maria Thomas | 219063060 | FT |

- Presentation
	- Date: 11/02/2021
	- Time: 16:00 - 16:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- comments
		- API gateway and API query with graphql
			- not really implemented
		- Kafka deployment
			- with a producer
		- logic within each service
			- nothing noteworthy
		- deployment with Kubernetes and Docker
			- not implemented (errors)
		- running implementation
			- partial (only Kafka)
- Mark:


## Group 13

- Repository: [Submission](https://github.com/makosaisaac/dspassignment2.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Isaac Makosa | 219005516 | FT |
| Takunda Hwaire | 219155380 | FT |
| Tafadzwa Taringa | 219003068 | FT |
| Adilson da Costa | 219020124 | FT |

- Presentation
	- Date: 10/02/2021
	- Time: 17:20 - 17:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
			- 219003068 did not show up
		
		
		- microservice design
			- several microservices were implemented (voting registration, campaign service, result)
			- no clear logic guiding the identification of the microservices
		- API gateway and API query with graphql
			- used graphql (but not properly implemented)
		- Kafka deployment
			- defined topics, a consumer and producer
		- logic within each service
			- nothing noteworthy
		- deployment with Kubernetes and Docker
			- attempted but failed
		- running implementation
			- partially (only the API gateway)
- Mark:

## Group 14

- Repository: [Submission](https://github.com/218064187/ass2-voto)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Olivier Lee-Roy | 218113951 | FT |
| Hango Ndeyapo | 218064187 | FT |
| Shilongo Stephen | 218059299 | FT |
| Bill Ikela | 218099509 | FT |

- Presentation
	- Date: 11/02/2021
	- Time: 09:00 - 09:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 1/02/2021
		
		
		- microservice design
			- Producer and consumer (all voting operation are implemented here)
			- client service
		- API gateway and API query with graphql
			- implemented
			- client querys consumer and producers through graphql
		- Kafka deployment
			- Consumer and Producers implemented
		- logic within each service
			- data is being saved to the map
		- deployment with Kubernetes and Docker
			- implemented but not running
		- running implementation
			- yes
- Mark:

## Group 15

- Repository: [Submission](https://github.com/)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Eliakim Haufiku | 215014928 | FT |

- Presentation
	- Date: 11/02/2021
	- Time: 17:00 - 17:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- barely attempted it
			- created two files
		- API gateway and API query with graphql
			- not working
		- Kafka deployment
			- deployed but no successful communication
		- logic within each service
			- nothing noteworthy
		- deployment with Kubernetes and Docker
			- not working
		- running implementation
			- partial (mostly the menu)
- Mark:

## Group 16

- Repository: [Submission](https://github.com/)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Vilho Nguno | 219068208 | FT |
| Vesta Ngomberume | 219085803 | FT |
| Vijanda V. Herunga | 216016681 | FT |
| Shania Nkusi | 219051704 | FT |

- Presentation
	- Date: 09/02/2021
	- Time: 12:00 - 12:20
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- InsertVoter Service
			- Display Voter service
		- API gateway and API query with graphql
			- not implemented
		- Kafka deployment
			- Consumer and Producer Implemented
		- logic within each service
			- data is being saved to a json file
		- deployment with Kubernetes and Docker
			- partially implemented
		- running implementation
			- no
- Mark:


## Group 17

- Repository: [Submission](https://github.com/Nessy-Nathingo/dsp-project-2)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Nestor Nathingo | 219051844 | FT |
| Sakaria Shilikomwenyo | 219055572 | FT |
| Simeon Hifikepunye | 219047987 | FT |
| Ronald Sangunji | 217069649 | FT |


- Presentation
	- Date: 12/02/2021
	- Time: 10:20 - 10:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
			- 219051844 did not show up
		
		
		- microservice design
			- only one service was identified
		- API gateway and API query with graphql
			- there's a component but it does not use GraphQL
		- Kafka deployment
			- with one producer and 3 consumers
		- logic within each service
			- no comments
		- deployment with Kubernetes and Docker
			- failed attempt
		- running implementation
			- not really (only compiles)
- Mark:


## Group 18

- Repository: [Submission](https://github.com/)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Florencia Kauhanda | 214000273 | PT |
| Khoa Goodwill | 219080534 | PT |
| Ushi Mukaru | 20010475 | PT |
| Vulikeni Kashikuka | 200442368 | PT |


- Presentation
	- Date: 11/02/2021
	- Time: 17:20 - 17:40
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- comments
		- API gateway and API query with graphql
			- comments
		- Kafka deployment
			- comments
		- logic within each service
			- comments
		- deployment with Kubernetes and Docker
			- coments
		- running implementation
			- yes/no
- Mark:

## Group 19

- Repository: [Submission](https://github.com/Ndapandula-cyber/Assignment-2)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Tabitha Nehale | 200840851 | FT |
| Shelma Shikongo | 218000146 | FT |
| Uajaa Veii | 200529994 | FT |
| Ndapandula Shiweva | 218072422 | FT |


- Presentation
	- Date: 11/02/2021
	- Time: 17:40 - 18:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: date
		
		
		- microservice design
			- created five services (voter registration, voter info, rejects, compute result)
			- the rationale of the decomposition is not clear
		- API gateway and API query with graphql
			- not attempted
		- Kafka deployment
			- wrong setup (changed ports instead of creating topics)
		- logic within each service
			- nothing noteworthy
		- deployment with Kubernetes and Docker
			- failed attempt
		- running implementation
			- no
- Mark:



## Group 20

- Repository: [Submission](https://github.com/WillyWonker69/DSP_ClassOf2020_ExclusiveIT-Ass2)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| Brave Wilson Kahweka |  219116458 | FT |
| Mino V. P. David | 219048355 | FT |
| Cesario D. C. Alves | 219063060 | FT |
| Oletu M. Shikomba |  219060355 | FT |


- Presentation
	- Date: 12/02/2021
	- Time: 14:40 - 15:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 31/01/2021
		
		
		- microservice design
			- Only one functionality is implemented vote registration and validation
		- API gateway and API query with graphql
			- No implemented 
		- Kafka deployment
			- There is a consumer and Producer
		- logic within each service
			- Nothing much
		- deployment with Kubernetes and Docker
			- Not implemented
		- running implementation
			- able to start kafka only
- Mark:

## Group 21

- Repository: [Submission](https://github.com/josemarlima/DSP_Assignment2.git)

- Members

| Name | Student Number| Study Mode |
|------|---------------|------------|
| U Hange |  200512579 | FT |
|  J Lima | 219011397 | FT |
| R Junia | 219041466 | FT |


- Presentation
	- Date: 15/02/2021
	- Time: 09:40 - 11:00
	- Comments:
		- repository
			- accessible: yes
			- latest commit: 12/02/2021
		
		Initial program crushed 
		- microservice design
			- add new voter
			- view Voter
			- candidates to display candidates have already registered
		- API gateway and API query with graphql
			- not implemented 
		- Kafka deployment
			- Producer and Consumer implemented
		- logic within each service
			- hardcoded data 
		- deployment with Kubernetes and Docker
			- Attempted 
		- running implementation
			- yes
- Mark:

